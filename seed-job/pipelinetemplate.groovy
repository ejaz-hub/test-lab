pipelineJob('pipeline-Env-2') {
  description('')
  displayName('pipeline-Env-2')
  keepDependencies(false)
  configure { flowdefinition ->
    flowdefinition / 'actions' << 'org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction'(plugin:'pipeline-model-definition')
    flowdefinition / 'actions' << 'org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction'(plugin:'pipeline-model-definition') {
      'jobProperties'()
      'triggers'()
      'parameters'()
      'options'()
    }
    flowdefinition << delegate.'definition'(class:'org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition',plugin:'workflow-cps@2.80') {
      'scm'(class:'hudson.plugins.git.GitSCM',plugin:'git@4.2.2') {
        'configVersion'(2)
        'userRemoteConfigs' {
          'hudson.plugins.git.UserRemoteConfig' {
            'url'('https://gitlab.com/ejaz-hub/test-lab.git')
            'credentialsId'('gitlab')
          }
        }
        'branches' {
          'hudson.plugins.git.BranchSpec' {
            'name'('*/master')
          }
        }
        'doGenerateSubmoduleConfigurations'(false)
        'submoduleCfg'(class:'list')
        'extensions'()
      }
      'scriptPath'('Default-Env.jdp')
      'lightweight'(true)
    }
  }
}
