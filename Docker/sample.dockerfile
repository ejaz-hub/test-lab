FROM alpine
RUN apk add bash && apk add apache2 
COPY index.html /var/www/html/
ADD http://s3.example.bucket/sample.demo /opt/
ENV homedir /home
ENV app_name demo
ENV username demo
ENV password redhat@123
USER abc
VOLUME [ "/mnt", "/var/www/html" ]
WORKDIR /opt/tomcat/
RUN touch sample.tomcat
EXPOSE 80
CMD [ "httpd", "DFOREGROUND" ]
CMD sleep 3000