# Docker-Online-B4

### Docker basic cmd
```docker
    docker pull tomcat
    docker pull tomcat:8.5.56-jdk11-openjdk-slim 
    docker images
    docker image rm tomcat
    docker container ls = docker ps
    docker container ls -a = docker ps -a
    docker run  nginx
    docker run  -d nginx
    docker run -d -p 8080:80 nginx
    docker run -d -P nginx
    docker run -d -P --name nginx_cloud nginx
    docker stop a71e9f5890d1
    docker rm a71e9f5890d1
    docker rm -f a71 // docker rm -f nginx_cloud{name}
    docker rm -f `docker ps -q`
    docker rm -f `docker ps -a -q`
    docker run -d --name sample-web nginx
    docker exec -it sample-web ip -a
    docker exec -it sample-web ip a
    docker exec -it sample-web hostname
    docker exec -it sample-web bash
    docker run -it nginx bash
```

### Docker volumes
```docker
     docker run -d -P -v /webapp/:/usr/share/nginx/html --name nginx-stable3 nginx   
        {
            syntax:
            docker run -d -P -v /container_externel_vol/:/container_internal_vol --name nginx-stable3 nginx 
        }

    docker volume create nginx-vol
    docker volume ls
    docker run -d -P -v nginx-vol:/usr/share/nginx/html --name nginx-stable3 nginx    
```

### Docker inspect
```dcocker
    docker inspect <containerid/name>
    docker image inspect <imagename/image_id>
    docker volume inspect <volume nane>
    docker network inspect <network_name>
```    

### Docker log
```docker    
    docker logs <container_id/container_name>

```
### Docker env
```docker 
    docker run -it -e db_username=root -e endpoint=app.localhost -e passwd=root@123 centos
    docker run -d -e key=value <image_name>
    docker run -d -e MYSQL_ROOT_PASSWORD=redhat123 mariadb
    docker run -d -e MYSQL_ROOT_PASSWORD=redhat123 -v /mysql/:/var/lib/mysql  mariadb
```
